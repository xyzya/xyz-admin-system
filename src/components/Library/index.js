// import BeatingHeart from './beatingHeart.vue'
// import Button from './button.vue'

// 导入library文件夹下的所有组件
// 批量导入需要使用一个函数 require.context(dir,deep,matching)
// 参数：1. 目录  2. 是否加载子目录  3. 加载的正则匹配

// 使用 require 提供的函数 context 加载某一个目录下的所有 .vue 后缀的文件。
// 然后 context 函数会返回一个导入函数 importFn
// 它又一个属性 keys() 获取所有的文件路径
// 通过文件路径数组，通过遍历数组，再使用 importFn 根据路径导入组件对象
// 遍历的同时进行全局注册即可

const importFn = require.context('./', false, /\.vue$/)

function plugin (Vue) {
  if (plugin.installed) {
    return
  }
  importFn.keys().forEach(key => {
    const comp = importFn(key).default
    Vue.component(comp.name, comp)
  })
}

export default plugin
