
/**
 *  通过 vue.extend 方法实现【自定义组件】【全局方法的调用】：
 *
 *  1. 通过Vue.extend来创建一个Vue的子类，传入一个组件对象
 *  2. 通过new来创建组件实例，
 *  3. new的时候可以传入组件的属性，比如data、methods、生命周期函数等等，它会和组件内定义的属性结合，而不会直接覆盖该属性。
    4. 然后通过 instance.$mount() 来渲染组件，当执行instance.$mount()时会渲染组件，但还没挂载到页面上
    5. 再通过.$el就拿到组件实际上的dom了，当然$mount()也可以挂载，可以参考项目中的main.js**
    6. 所以上面的挂载到body上面也可以写成: instance.$mount('body')

 */
import Vue from 'vue'
import DialogCom from './dialog.vue'
import MessageCom from './message.vue'

// ========== dialog =============
// 1. 传入一个组件对象，通过Vue.extend来创建一个Vue的子类
const DialogConstructor = Vue.extend(DialogCom)
let dialogInstance

const Dialog = function (options = {}) {
  // 2. 通过new来创建组件实例
  dialogInstance = new DialogConstructor({
  // 3. 可以传入组件的属性，比如data、methods、生命周期函数等等
    data: options
  })
  // 4. 通过 instance.$mount() 来渲染组件
  // 5. 再通过.$el就拿到组件实际上的dom实现挂载了
  document.body.appendChild(dialogInstance.$mount().$el)
}

// ========== message =============
const MessageConstructor = Vue.extend(MessageCom)
let msgInstance

const messageTip = function (options = {}) {
  msgInstance = new MessageConstructor({
    data: options
  })
  document.body.appendChild(msgInstance.$mount().$el)
}

const install = (Vue, opts = {}) => {
  Vue.prototype.$MessageTip = messageTip
  Vue.prototype.$Dialog = Dialog
}

export default install
