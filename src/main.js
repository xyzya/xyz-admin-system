import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui' // 引入所有elementUI组件
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss' // global css
import './permission'
import Library from '@/components/Library' // 引入自定义全局组件
// import Dialog from '@/components/Element/extend.js' // 引入封装过的全局组件的调用方法
import install from '@/components/Element/extend.js' // 引入封装过的全局组件的调用方法
import '@/icons' // icon

// Vue.prototype.$Dialog = Dialog // 把全局组件的调用方法挂载到vue原型上
// Vue.prototype.$MessageTip = MessageTip // 把全局组件的调用方法挂载到vue原型上

Vue.use(ElementUI)
Vue.use(Library)
Vue.use(install)

if (process.env.NODE_ENV === 'development') {
  const { mockXHR } = require('../mock')
  mockXHR()
}


// 实现拖拽的Vue指令
Vue.directive('drag',el=>{
  const oDiv = el // 当前元素
  const minTop = oDiv.getAttribute('drag-min-top')
  const ifMoveSizeArea = 20

  oDiv.onmousemove=e=>{
    let target = oDiv
    while(window.getComputedStyle(target).position !== ' absolute'){
      target = target.parentElement
    }
  }
})


Vue.config.productionTip = false

// runtime 模式
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// compiler 模式
// new Vue({
//   el: '#app',
//   router: router,
//   store: store,
//   template: '<App/>',
//   components: { App }
// })
