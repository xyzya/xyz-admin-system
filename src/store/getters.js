const getters = {
  sidebar: state => state.app.sidebar,
  roles: state => state.user.roles,
  token: state => state.user.token,
  name: state => state.user.name,
  avatar: state => state.user.avatar,
  introduction: state => state.user.introduction,
  permission_routes: state => state.permission.routes

}

export default getters
