import router from './router'
import store from './store'
// import { Message } from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getToken } from '@/utils/auth'
import getPageTitle from '@/utils/get-page-title'
import { Message } from 'element-ui'

NProgress.configure({ showSpinner: false })

// 不需要重定向的白名单
const whiteList = ['/login', '/auth-redirect']

// 路由跳转前拦截
router.beforeEach(async (to, from, next) => {
  // console.log('to', to)
  // 1. 进度条开始
  NProgress.start()

  // 2. 设置页面标题
  document.title = getPageTitle(to.meta.title)

  // 3. 判断用户是否已经登录
  const hasToken = getToken()
  // console.log('hasToken', hasToken)

  if (hasToken) { // 已登录
    if (to.path === '/login') {
      // 3.1 目标为登录页，状态为已登录，则重定向到主页
      next({ path: '/' })
      NProgress.done()
    } else {
      // 3.2 目标是其他页面
      // 判断用户是否通过 getInfo 获得了权限角色
      const hasRoles = store.getters.roles && store.getters.roles.length > 0
      // console.log('store.getters.roles', store.getters.roles)
      // console.log('hasRoles', hasRoles)
      if (hasRoles) {
        // 有权限
        next()
      } else {
        // 没有权限
        try {
          // 调用 getInfo 方法，获取权限
          const { roles } = await store.dispatch('user/getInfo')
          // console.log('roles', roles)

          // 根据roles算出可以访问的路由
          const assessRoutes = await store.dispatch('permission/generateRoutes', roles)

          // 动态添加可以访问的路由
          router.addRoutes(assessRoutes)
          next({ ...to, replace: true })
        } catch (error) {
          // 清除token，重定向回登录页重新登录
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`./login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    // 4.未登录
    if (whiteList.includes(to.path)) {
      // 白名单内的页面直接跳转
      next()
    } else {
      // 非白名单的页面跳到登录页
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
