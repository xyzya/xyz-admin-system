import Cookies from 'js-cookie'

const TokeyKey = 'Admin-Token'

export function getToken () {
  return Cookies.get(TokeyKey)
}

export function setToken (token) {
  return Cookies.set(TokeyKey, token)
}

export function removeToken () {
  return Cookies.remove(TokeyKey)
}
