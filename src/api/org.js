import request from '@/utils/request'

export function getOrgData (data) {
  return request({
    url: '/vue-element-admin/charts/org',
    method: 'post',
    data
  })
}

export function uploadFile (data, onUploadProgress = () => {}) {
  return request({
    url: '/vue-element-admin/uploadFile',
    method: 'post',
    data,
    onUploadProgress
  })
}

export function mergeChunks (data) {
  return request({
    url: '/vue-element-admin/mergeChunks',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
