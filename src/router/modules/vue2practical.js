import Layout from '@/layout'

const vuepractical = {
  path: '/vuepractical',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Practical',
  meta: {
    title: 'Vue2实操',
    icon: 'el-icon-s-grid'
  },
  children: [
    {
      path: 'globalAPI',
      component: () => import('@/views/vuepractical/globalAPI.vue'),
      name: 'Extend',
      meta: {
        title: 'vue全局API',
        icon: 'el-icon-s-grid'
      }
    },
    {
      path: 'functionalCom',
      component: () => import('@/views/vuepractical/functional.vue'),
      name: 'FunctionalCom',
      meta: {
        title: '函数式组件',
        icon: 'el-icon-s-grid'
      }

    },
    {
      path: 'vModel',
      component: () => import('@/views/vuepractical/vModel.vue'),
      name: 'VModel',
      meta: {
        title: 'VModel',
        icon: 'el-icon-s-grid'
      }

    },
    {
      path: 'drag',
      component: () => import('@/views/vuepractical/drag.vue'),
      name: 'Drag',
      meta: {
        title: 'Drag',
        icon: 'el-icon-s-grid'
      }

    }
    
  ]
}

export default vuepractical
