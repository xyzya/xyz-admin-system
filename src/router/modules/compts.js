import Layout from '@/layout'

const comptsRouter = {
  path: '/compts',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Compts',
  meta: {
    title: '组件',
    icon: 'el-icon-s-grid'
  },
  children: [
    {
      path: 'fallingStar',
      component: () => import('@/views/compts/fallingStar'),
      name: 'FallingStar',
      meta: {
        title: '动态流星', icon: 'el-icon-money'
      }
    },
    {
      path: 'dynamicBg',
      component: () => import('@/views/compts/dynamicBg'),
      name: 'DynamicBg',
      meta: {
        title: '动态背景', icon: 'el-icon-money'
      }
    },
    {
      path: 'beatingHeart',
      component: () => import('@/views/compts/beatingHeart'),
      name: 'BeatingHeart',
      meta: {
        title: '跳动的心', icon: 'el-icon-money'
      }
    },
    {
      path: 'cssAnimation',
      component: () => import('@/views/compts/cssAnimation'),
      name: 'CssAnimation',
      meta: {
        title: 'CSS动画', icon: 'el-icon-money'
      }
    },
    {
      path: 'smallComs',
      component: () => import('@/views/compts/smallComs'),
      name: 'SmallComs',
      meta: {
        title: '小组件', icon: 'el-icon-money'
      }
    },
    {
      path: 'goodsDetails',
      component: () => import('@/views/compts/goodsDetail'),
      name: 'GoodsDetails',
      meta: {
        title: '商品详情', icon: 'el-icon-money'
      }
    },
    {
      path: 'pageBuild',
      component: () => import('@/views/compts/pageBuild'),
      name: 'PageBuild',
      meta: {
        title: '页面构建', icon: 'el-icon-money'
      }
    }

  ]
}

export default comptsRouter
