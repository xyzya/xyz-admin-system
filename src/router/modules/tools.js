import Layout from '@/layout'

const toolsRouter = {
  path: '/tools',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Tools',
  meta: {
    title: '工具',
    icon: 'el-icon-s-grid'
  },
  children: [
    {
      path: 'bigFileUpload',
      component: () => import('@/views/tools/bigFileUpload'),
      name: 'BigFileUpload',
      meta: {
        title: '大文件上传', icon: 'el-icon-money'
      }
    }
  ]
}
export default toolsRouter
