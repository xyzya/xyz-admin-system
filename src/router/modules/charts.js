import Layout from '@/layout'

const chartsRouter = {
  path: '/charts',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Charts',
  meta: {
    title: 'Charts',
    icon: 'el-icon-s-marketing'
  },
  children: [
    {
      path: 'keyboard',
      component: () => import('@/views/charts/keyboard'),
      name: 'KeyboardChart',
      meta: { title: 'Keyboard Chart', icon: 'el-icon-s-tools', noCache: true }
    },
    {
      path: 'line',
      component: () => import('@/views/charts/line'),
      name: 'LineChart',
      meta: { title: 'Line Chart', icon: 'el-icon-s-management', noCache: true }
    },
    {
      path: 'org',
      component: () => import('@/views/charts/org'),
      name: 'Org',
      meta: { title: '组织架构', icon: 'el-icon-s-management', noCache: true }
    }
  ]
}

export default chartsRouter
