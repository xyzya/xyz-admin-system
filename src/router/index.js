import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '@/views/Home.vue'
import Layout from '@/layout'

import chartsRouter from './modules/charts'
import comptsRouter from './modules/compts'
import toolsRouter from './modules/tools'
import vue2practical from './modules/vue2practical'
Vue.use(VueRouter)

export const asyncRoutes = [
  chartsRouter,
  comptsRouter,
  toolsRouter,
  vue2practical
]

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login'),
    hidden: true
  },
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'Dashboard', icon: 'el-icon-s-grid', affix: true }
      }
    ]
  }
]

// 写法一
// const creatRouters = () => new VueRouter({
//   routes:constantRoutes
// })
// const router = creatRouters()

// 写法二
const router = new VueRouter({
  routes: constantRoutes
})

export default router
