
// import { organizationData } from './data/orgData'
const { organizationData } = require('./data/orgData.js')

module.exports = [
  // user login
  {
    url: '/vue-element-admin/charts/org',
    type: 'post',
    response: organizationData
  }
]
