// 组织架构数据
const organizationData = {
  intRes: 0,
  msgRes: '成功',
  objRes: [
    {
      orgUnitID: 11,
      orgUnitCode: '11-',
      orgUnitName: '中国南方航空集团有限公司',
      orgUnitParentNames: '',
      disabled: false,
      children: [
        {
          orgUnitID: 85,
          orgUnitCode: '11-85-',
          orgUnitName: '中国南航集团文化传媒股份有限公司',
          orgUnitParentNames: '中国南方航空集团有限公司',
          disabled: false,
          children: [
            {
              orgUnitID: 6690,
              orgUnitCode: '11-85-6690-',
              orgUnitName: '南航文化传媒（北京）有限公司',
              orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司',
              disabled: false,
              children: [
                {
                  orgUnitID: 61952,
                  orgUnitCode: '11-85-6690-61952-',
                  orgUnitName: '频道事业部',
                  orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司-南航文化传媒（北京）有限公司',
                  disabled: false,
                  children: []
                },
                {
                  orgUnitID: 61954,
                  orgUnitCode: '11-85-6690-61954-',
                  orgUnitName: '销售部',
                  orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司-南航文化传媒（北京）有限公司',
                  disabled: false,
                  children: []
                }
              ]
            },
            {
              orgUnitID: 4678,
              orgUnitCode: '11-85-4678-',
              orgUnitName: '人力资源部',
              orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司',
              disabled: false,
              children: []
            },
            {
              orgUnitID: 6585,
              orgUnitCode: '11-85-6585-',
              orgUnitName: '南航文化传媒北方有限公司',
              orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司',
              disabled: false,
              children: [
                {
                  orgUnitID: 6619,
                  orgUnitCode: '11-85-6585-6619-',
                  orgUnitName: '子公司领导',
                  orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司-南航文化传媒北方有限公司',
                  disabled: false,
                  children: []
                },
                {
                  orgUnitID: 6621,
                  orgUnitCode: '11-85-6585-6621-',
                  orgUnitName: '品牌服务部',
                  orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司-南航文化传媒北方有限公司',
                  disabled: false,
                  children: []
                }
              ]
            },
            {
              orgUnitID: 2001,
              orgUnitCode: '11-85-2001-',
              orgUnitName: '公司领导',
              orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司',
              disabled: false,
              children: []
            },
            {
              orgUnitID: 62033,
              orgUnitCode: '11-85-62033-',
              orgUnitName: '广州市广天合传媒有限公司',
              orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司',
              disabled: false,
              children: [
                {
                  orgUnitID: 4588,
                  orgUnitCode: '11-85-62033-4588-',
                  orgUnitName: '采编中心',
                  orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司-广州市广天合传媒有限公司',
                  disabled: false,
                  children: [
                    {
                      orgUnitID: 61921,
                      orgUnitCode: '11-85-62033-4588-61921-',
                      orgUnitName: '中心领导',
                      orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司-广州市广天合传媒有限公司-采编中心',
                      disabled: false,
                      children: []
                    },
                    {
                      orgUnitID: 61922,
                      orgUnitCode: '11-85-62033-4588-61922-',
                      orgUnitName: '员工版',
                      orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司-广州市广天合传媒有限公司-采编中心',
                      disabled: false,
                      children: []
                    },
                    {
                      orgUnitID: 61923,
                      orgUnitCode: '11-85-62033-4588-61923-',
                      orgUnitName: '航机版 ',
                      orgUnitParentNames: '中国南方航空集团有限公司-中国南航集团文化传媒股份有限公司-广州市广天合传媒有限公司-采编中心',
                      disabled: false,
                      children: []
                    }
                  ]
                }
              ]

            }
          ]
        }

      ]
    }

  ]
}

module.exports = { organizationData }
