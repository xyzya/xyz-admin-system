const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}
const port = 9530 // dev port
module.exports = {
  publicPath: process.env.VUE_APP_PUBLIC_PATH,
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: 'http://localhost:9931',
        // target: 'http://10.79.7.29:9668',
        // target: `https://ehmtest.csair.com`,
        // target: 'http://192.168.1.153:8011',
        wx: true,
        changeOrigin: true
      }
    }
  },
  runtimeCompiler: true,
  // configureWebpack: {
  //   resolve: {
  //     alias: {
  //       vue$: 'vue/dist/vue.esm.js'
  //     }
  //   }
  // },
  chainWebpack (config) {
    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  }
}
